moved {
  from = digitalocean_droplet.target
  to   = module.jumphost.digitalocean_droplet.target
}

moved {
  from = digitalocean_firewall.target
  to   = module.jumphost.digitalocean_firewall.target
}

moved {
  from = digitalocean_ssh_key.target
  to   = module.ssh_key.digitalocean_ssh_key.target
}

moved {
  from = tls_private_key.target
  to   = module.ssh_key.tls_private_key.target
}

moved {
  from = local_file.ssh_key
  to   = module.ssh_key.local_file.ssh_key
}
