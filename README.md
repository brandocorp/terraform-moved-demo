# Terraform Moved Demo

This repository is meant to be an example of using the `moved` syntax to refactor a Terraform module.

# Stage 0

In the first stage, we start off with a simple module that creates a Digital Ocean VPC, a droplet, an SSH key, and a firewall rule. I believe this is how many modules start out their lives, and so it should serve as a good base for refactoring.

# Stage 1

```
git checkout stage1
```

We start our refactoring by extracting the droplet out into its own module. A jumphost is a thing we might need more than one of, and the requirements will likely be very similar for each instance, so it seems like a good fit for refactoring.

# Stage 2

```
git checkout stage2
```

We continue our refactoring by extracting the firewall rule. This, too, will be a common resource required whenever we create a new jumphost, so it should be pulled up and out into the jumphost module.

# Stage 3

```
git checkout stage3
```

Looking at our module, it seems we have another clump of resources we will almost always be creating together. The `tls_private_key`, `digitalocean_ssh_key`, and the `local_file` resources should be extracted to their own SSH key module to make them reusable for additional jumphosts.
 