terraform {
  # moved block was introduced in v1.1.0
  required_version = ">= 1.1.0"

  required_providers {
    digitalocean = {
      source = "digitalocean/digitalocean"
      version = "~> 2.17.0"
    }
  }
}
