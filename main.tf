provider "digitalocean" {}

locals {
  name   = "jumphost"
  region = "sfo3"
}

data "digitalocean_project" "target" {
  name = "terraform-moved-demo"
}

resource "digitalocean_vpc" "target" {
  name     = "terraform-moved-demo"
  region   = local.region
  ip_range = "10.10.10.0/24"
}

module "ssh_key" {
  source = "./modules/terraform-digitalocean-ssh-key"

  name = local.name
}

module "jumphost" {
  source = "./modules/terraform-digitalocean-jumphost"

  name     = local.name
  size     = "s-1vcpu-1gb"
  image    = "ubuntu-20-04-x64"
  region   = local.region
  vpc_uuid = digitalocean_vpc.target.id
  ssh_keys = [module.ssh_key.fingerprint]
}

resource "digitalocean_project_resources" "target" {
  project = data.digitalocean_project.target.id
  resources = [
    module.jumphost.urn
  ]
}
