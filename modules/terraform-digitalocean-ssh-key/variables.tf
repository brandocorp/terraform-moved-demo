variable "name" {
  description = "The name of the SSH key."
  type        = string
}
