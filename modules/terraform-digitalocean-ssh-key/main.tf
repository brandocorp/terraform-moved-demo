resource "tls_private_key" "target" {
  algorithm   = "ECDSA"
  ecdsa_curve = "P256"
}

resource "local_file" "ssh_key" {
  filename          = "${var.name}.pem"
  file_permission   = "0600"
  sensitive_content = tls_private_key.target.private_key_pem
}

resource "digitalocean_ssh_key" "target" {
  name       = var.name
  public_key = tls_private_key.target.public_key_openssh
}
