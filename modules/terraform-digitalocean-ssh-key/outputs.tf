output "fingerprint" {
  description = "The fingerprint of the SSH key."
  value       = digitalocean_ssh_key.target.fingerprint
}
