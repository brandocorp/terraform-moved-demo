data "digitalocean_image" "target" {
  slug = var.image
}

resource "digitalocean_droplet" "target" {
  name     = var.name
  size     = var.size
  image    = data.digitalocean_image.target.slug
  region   = var.region
  vpc_uuid = var.vpc_uuid
  ssh_keys = var.ssh_keys
}

resource "digitalocean_firewall" "target" {
  name = "${var.name}-ssh"

  droplet_ids = [digitalocean_droplet.target.id]

  inbound_rule {
    protocol         = "tcp"
    port_range       = "22"
    source_addresses = ["0.0.0.0/0", "::/0"]
  }

  inbound_rule {
    protocol         = "icmp"
    source_addresses = ["0.0.0.0/0", "::/0"]
  }

  outbound_rule {
    protocol              = "tcp"
    port_range            = "53"
    destination_addresses = ["0.0.0.0/0", "::/0"]
  }

  outbound_rule {
    protocol              = "udp"
    port_range            = "53"
    destination_addresses = ["0.0.0.0/0", "::/0"]
  }

  outbound_rule {
    protocol              = "icmp"
    destination_addresses = ["0.0.0.0/0", "::/0"]
  }
}
