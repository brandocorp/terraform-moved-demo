variable "image" {
  type = string
  description = "The digitalocean image slug."
}

variable "name" {
  type        = string
  description = "The name of the jumphost."
}

variable "region" {
  type        = string
  description = "The digitalocean region."
}

variable "ssh_keys" {
  type        = list(string)
  description = "A list of SSH key IDs or fingerprints."
}

variable "size" {
  type        = string
  description = "The digitalocean size."
}

variable "vpc_uuid" {
  type        = string
  description = "The digialocean VPC UUID."
}
