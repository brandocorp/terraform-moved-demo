output "id" {
  description = "The ID of the jumphost."
  value       = digitalocean_droplet.target.id
}

output "name" {
  description = "The name of the jumphost."
  value       = digitalocean_droplet.target.name
}

output "urn" {
  description = "The uniform resource name of the jumphost."
  value       = digitalocean_droplet.target.urn
}
