# terraform-digitalocean-jumphost

A Terraform module used to create an SSH Jumphost within a VPC.

## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_digitalocean"></a> [digitalocean](#requirement\_digitalocean) | ~> 2.17.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_digitalocean"></a> [digitalocean](#provider\_digitalocean) | ~> 2.17.0 |

## Resources

| Name | Type |
|------|------|
| [digitalocean_droplet.target](https://registry.terraform.io/providers/digitalocean/digitalocean/latest/docs/resources/droplet) | resource |
| [digitalocean_firewall.target](https://registry.terraform.io/providers/digitalocean/digitalocean/latest/docs/resources/firewall) | resource |
| [digitalocean_image.target](https://registry.terraform.io/providers/digitalocean/digitalocean/latest/docs/data-sources/image) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_image"></a> [image](#input\_image) | The digitalocean image slug. | `string` | n/a | yes |
| <a name="input_name"></a> [name](#input\_name) | The name of the jumphost. | `string` | n/a | yes |
| <a name="input_region"></a> [region](#input\_region) | The digitalocean region. | `string` | n/a | yes |
| <a name="input_size"></a> [size](#input\_size) | The digitalocean size. | `string` | n/a | yes |
| <a name="input_ssh_keys"></a> [ssh\_keys](#input\_ssh\_keys) | A list of SSH key IDs or fingerprints. | `list(string)` | n/a | yes |
| <a name="input_vpc_uuid"></a> [vpc\_uuid](#input\_vpc\_uuid) | The digialocean VPC UUID. | `string` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_id"></a> [id](#output\_id) | The ID of the jumphost. |
| <a name="output_name"></a> [name](#output\_name) | The name of the jumphost. |
| <a name="output_urn"></a> [urn](#output\_urn) | The uniform resource name of the jumphost. |